class Api {

    constructor(host = 'http://localhost:3000/list') {
        this.host = host;
    }

    getList() {
        return fetch(this.host)
            .then(res => res.json())
            .then(json => json);
    }

    addElement(element) {
        return fetch(this.host, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(element)
        }).then(resp => {
            if (resp.ok) {
                return resp.json();
            }
        });
    }

    removeElement(id) {
        return fetch(`${this.host}/${id}`, {
            method: 'DELETE'
        });
    }


}

export default Api;