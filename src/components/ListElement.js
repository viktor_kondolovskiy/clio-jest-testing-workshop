import React from 'react';

function ListElement({ element, removeHandler }) {

    const handleRemove = () => {
        removeHandler(element.id);
    };

    return (
        <li>
            <span>{element.text}</span>
            &nbsp;
            <button onClick={handleRemove}>X</button>
        </li>
    );
}

export default ListElement;