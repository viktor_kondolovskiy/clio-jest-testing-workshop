import React from 'react';

import ListElement from './ListElement';

function TodoList({ list, removeElement }) {

    const listElements = list.map(element => <ListElement
        removeHandler={removeElement}
        element={element}
        key={element.id}
    />);

    return (
        <div>
            <ul>
                {listElements}
            </ul>
        </div>
    );

}

export default TodoList;