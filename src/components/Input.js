import React, { useState } from 'react';

function Input({ addTodo }) {

    const [text, setText] = useState('');

    const handleChange = event => setText(event.target.value);

    const handelSubmit = event => {
        event.preventDefault();
        addTodo(text);
        setText('');
    };

    return (
        <div>
            <form onSubmit={handelSubmit}>
                <input autoComplete={"off"} className="input-add" name="add-todo" onChange={handleChange} value={text} />
                <input className="btn-submit" type="submit" value="Add todo" />
            </form>
        </div>
    );

}

export default Input;