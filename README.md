# Clio academy workshop: Testing with JEST

## Description

This is a simple TODO application for jest testing workshop

## Installation
1. Make sure than `localhost:3000` and `localhost:8080` are available
2. Run `npm install` and open `http://localhost:8080`

## Testing

1. Run `npm test` to run all tests
2. Run `npm run test:watch` to run tests in watch mode
3. Run `npm run coverage` to run all tests with coverage




