import React, { useEffect } from 'react';
import Input from './Input';
import TodoList from './TodoList';

import useTodo from '../hooks/todo';
import LocalStorage from '../services/LocalStorage';
import Api from '../services/Api';

function App() {

    const store = new LocalStorage('todoList');

    const api = new Api();

    const [todoList, methods] = useTodo(store.items || []);

    useEffect(() => {
        store.items = todoList;
    }, [todoList]);

    useEffect(() => {
        api.getList().then(result => methods.setTodos(result));
    }, []);

    const addTodo = text => {
        const element = {
            text
        };
        api.addElement(element).then(createdElement => {
            methods.addTodo(createdElement);
        });
    };

    const removeTodo = id => {
        api.removeElement(id).then(() => methods.removeTodo(id));
    };

    return (
        <div className="app-wrapper">
            <Input addTodo={addTodo}/>
            <TodoList list={todoList} removeElement={removeTodo}/>
        </div>
    );
}

export default App;