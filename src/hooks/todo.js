import React, { useState } from 'react';

import { addElement, removeElement } from "../helpers/arrayHelpers";

function useTodo(initialArray = []) {

    const [todoList, setTodoList] = useState(initialArray);

    const addTodo = element => {
        if (element.id && element.text.length) {
            const newList = addElement(todoList, element);
            setTodoList(newList);
        }
    };

    const removeTodo = id => {
        const newList = removeElement(todoList, id);
        setTodoList(newList);
    };

    const setTodos = list => setTodoList(list);

    return [
        todoList,
        {
            addTodo, removeTodo, setTodos
        }
    ];
}

export default useTodo;