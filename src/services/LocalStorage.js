class LocalStorage {

    constructor(key) {
        this.key = key;
    }

    setItem(data) {
        localStorage.setItem(this.key, JSON.stringify(data));
    }

    getItem() {
        return JSON.parse(localStorage.getItem(this.key) || null);
    }

    set items(data) {
        this.setItem(data);
    }

    get items() {
        return this.getItem();
    }
}

export default LocalStorage;