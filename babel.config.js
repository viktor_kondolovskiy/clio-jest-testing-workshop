module.exports = {
    presets: [
        [
            '@babel/preset-env',
            {
                corejs: 3,
                targets: {
                    node: 'current',
                },
            },
        ],
    ],
    plugins: [
        "@babel/transform-runtime"
    ]
};