/**
 * Add element to existed array
 * @param {Array} arr - array the element should be added to
 * @param {Object} element - a new element should be added
 * @return {Array}
 */
function addElement(arr, element) {
    if (!element.id) {
        throw new Error('Element should be an object and contact id');
    }
    return arr.concat(element);
}

/**
 * Remove element from array
 * @param {Array} arr - target array
 * @param {String} id - id of element to remove
 */
function removeElement(arr, id) {
    return arr.filter(el => el.id !== id);
}

/**
 * Get element from array
 * @param {Array} arr - target array
 * @param {String} id - id of target element
 */
function findElement(arr, id) {
    const elements =  arr.filter(el => el.id === id);

    if (elements.length > 1) {
        throw new Error("Something wrong with data provider: found more than one element");
    }

    return elements[0] ? elements[0] : null;
}

export {
    addElement,
    removeElement,
    findElement
}